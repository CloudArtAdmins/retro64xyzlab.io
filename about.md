---
layout: page
title: About
permalink: /about/
---
{::options parse_block_html="true" /}
<div class="author-img"> 
![Aaron (Retro64XYZ)][aaronimg]
</div>
{::options parse_block_html="false" /}

Aaron, the owner of Retro64XYZ, is a software developer who currently creates
applications for law enforcement. He is also an AZ POST certified public
speaker. He earned a B.Sc., in Computer Information Systems from Park
University in 2013 and an M.A., in Intelligence Analysis with a focus in Cyber
Security in 2014. During that period of his life he took a double course load
and completed his Masters with a 3.695 GPA in a year. He has been the recipient
of recognition from the El Paso Police Department, State Of Texas, Texas
Military Forces, Chandler Police Department, and others.

Aaron is also active in the community as the founder of the Phoenix Linux Users
Group Cyber Security Meetup and regularly teaches members of the public a
myriad of topics related to Cyber Security. His audience includes students,
teachers, law enforcement, military, government officials, and concerned
members of the public with a strong desire to learn what is going on in the
world of technology.

When Aaron isn't teaching, working, or spending time with his family, he enjoys
relaxing at the pond with a fishing pole while not catching fish, operating a
pistol at the shooting range, or reading books. He owns a Sega Saturn and a
Sega Dreamcast and his favorite video games are Panzer Dragoon, Road Rash,
Phantasy Star Online 2, and Power Stone. He is currently engrossed in building
content for his site and looking for more ways to reach the public. You should
reach Aaron through his [Mastodon][mastodon] or on [Keybase][keybase]. He would
love to hear from you, answer your questions, or find out about the projects
you are involved with.

[mastodon]: https://mastodon.xyz/@{{site.mastodon_username}}
[keybase]: https://keybase.io/{{site.keybase_username}}
[aaronimg]: /../assets/images/inserts/about/aaron.jpg
